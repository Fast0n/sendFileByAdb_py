from tkinter import filedialog
from tkinter import *
from strings import *
import tkinter.messagebox
import os


def browse_button():
    check_device()
    global folder_path

    selection = str(varr.get())
    if selection == "1":
        filename = filedialog.askdirectory()
    elif selection == "2":
        filename = filedialog.askopenfilename(multiple=1)

        filename = " ".join([str(x) for x in filename])
    folder_path.set(filename)
    return filename


def check_device():
    global device_info
    try:
        device = os.popen("adb devices").read()
        info = device.split('\n')
        device = info[1].split("\t")[1]
        if device == "device":
            select.config(state=NORMAL)
            send.config(state=NORMAL)
            root.title(py_name + " - Device Found " + info[1].split("\t")[0])
            device_info.set("Device Found " + info[1].split("\t")[0])
    except IndexError:
        device_info.set('List of devices attached')
        root.title(py_name + " - No Device")
        select.config(state=DISABLED)
        send.config(state=DISABLED)


def send_file():
    check_device()
    if folder_path.get().replace(" ", "\ ") == "":
        tkinter.messagebox.showwarning(py_name, file_not_found)
        return check_device

    try:
        device = os.popen("adb devices").read()
        info = device.split('\n')
        device = info[1].split("\t")[1]
        if device == "device":

            os.system("clear && adb push " +
                      folder_path.get().replace(" ", "\ ").replace("\ /", " /") + " /sdcard/")

            tkinter.messagebox.showwarning(py_name, send_sucessfull)

    except IndexError:
        print ('List of devices attached')
        root.title(py_name + " - No Device")
        select.config(state=DISABLED)
        send.config(state=DISABLED)
        tkinter.messagebox.showwarning(py_name, device_not_found)


# window
root = Tk()
root.minsize(500, 250)

# label
var = StringVar()
label = Label(root, textvariable=var)
var.set(welcome + py_name)
label.grid(row=0, column=1)

#--check_device
check = Button(text=check, command=check_device)
check.grid(row=1, column=1)

#--select
select = Button(text=browse, command=browse_button)
select.grid(row=2, column=1)

# radiobutton folder
varr = IntVar()
r1 = Radiobutton(root, text=folder, variable=varr, value=1, tristatevalue="x")
r1.grid(row=3, column=0)

# radiobutton file
r2 = Radiobutton(root, text=file, variable=varr, value=2)
r2.grid(row=3, column=2)

#--send
send = tkinter.Button(root, text=send, command=send_file)
send.grid(row=4, column=1)

#--quit
quit = tkinter.Button(root, text=quit, command=root.quit)
quit.grid(row=5, column=1)

# device
device_info = StringVar()
deviceinfo = Label(master=root, textvariable=device_info, relief=RAISED)
deviceinfo.grid(row=6, column=1)

# label address
folder_path = StringVar()
label = Label(master=root, textvariable=folder_path)
label.grid(row=7, column=1)

# run
check_device()
address = os.popen("pwd").read()
info = address.split('\n')[0]
folder_path.set(info)
varr.set(2)
mainloop()